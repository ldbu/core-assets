/*
 * MIT License
 *
 * Copyright (c) 2016 Thought Logix
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

var gulp = require("gulp");
var reload = require("browser-sync").reload;
var clean = require('gulp-clean');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var minifycss = require('gulp-minify-css');
var imagemin = require('gulp-imagemin');
var changed = require('gulp-changed');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var plumber = require('gulp-plumber');
var concat = require('gulp-concat');
var multiDest = require('gulp-multi-dest');

var assetsDir = '../src/main/resources/public/core/assets/';

function handleError(err) {
    console.log(err.toString());
    this.emit('end');
}

gulp.task('clean', function () {
    return gulp.src([assetsDir], {read: false})
        .pipe(clean({force: true}));
});

gulp.task('fonts', function () {
    return gulp.src(['src/fonts/**/*'])
        .pipe(multiDest([assetsDir + '/fonts']));
});

gulp.task('images', function () {
    return gulp.src('src/img/**/*')
        .pipe(plumber({errorHandler: handleError}))
        .pipe(changed(assetsDir + '/img'))
        .pipe(imagemin())
        .pipe(multiDest([assetsDir + '/img']))
});

gulp.task('scripts', function () {
    return gulp.src([
        'src/js/*.js'])
        .pipe(plumber({errorHandler: handleError}))
        // .pipe(jshint())
        .pipe(concat('core.min.js'))
        .pipe(jshint.reporter("default"))
        .pipe(uglify())
        .pipe(multiDest([assetsDir + '/js']))
});

gulp.task('styles', function () {
    return gulp.src('src/css/main.scss')
        .pipe(plumber({errorHandler: handleError}))
        .pipe(sass())
        .pipe(autoprefixer('last 2 version'))
        .pipe(minifycss({advanced: true}))
        .pipe(concat('core.min.css'))
        .pipe(multiDest([assetsDir + '/css']))
});

gulp.task('svg', function () {
    return gulp.src('src/svg/*.svg')
    //    .pipe(svgmin())
        .pipe(multiDest([assetsDir + '/img']))
});

gulp.task('watch', ['build'], function () {
    gulp.watch(['src/css/**'], ['styles']);
    gulp.watch(['src/js/**'], ['scripts']);
    gulp.watch(['src/fonts/**'], ['fonts']);
    gulp.watch(['src/img/**', 'src/svg/*.svg'], ['images']);
    gulp.watch(['src/svg/**'], ['svg']);
});


gulp.task('build', ['styles', 'scripts', 'images', 'svg', 'fonts'], reload);

gulp.task('default', ['build']);
